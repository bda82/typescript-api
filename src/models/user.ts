import { Model, Sequelize, DataTypes } from 'sequelize';

export default class User extends Model {
    public id?: number;
    public firstname!: string;
    public lastname!: string;
    public email!: string;
    public phone!: string;
}

export const UserMap = (sequelize: Sequelize) => {
    User.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        firstname: {
            type: DataTypes.STRING(255)
        },
        lastname: {
            type: DataTypes.STRING(255),
        },
        email: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        phone: {
            type: DataTypes.STRING(32),
            allowNull: true
        }
    }, {
        sequelize,
        tableName: 'users',
        timestamps: false
    });
    User.sync();
}