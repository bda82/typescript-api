import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import usersRoutes from './routes/user.routes';

const PORT = process.env.PORT || 3001

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req: Request, res: Response) => {
    res.send('afasdffas')
})

app.use('/users', usersRoutes);

app.listen(PORT, () => {
    console.log(`app running on port ${PORT}`);
})