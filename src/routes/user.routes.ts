import User, { UserMap } from '../models/user';
import database from '../db'
import { Request, Response, Router } from 'express';


const router = Router();

router.get('/', async (req: Request, res: Response) => {
    console.log('GET /users request')
    UserMap(database);
    const result = await User.findAll();
    res.status(200).json({ users: result });
});

router.get('/:id', async (req: Request, res: Response) => {
    console.log('GET /users/:id request')
    UserMap(database);
    const id = Number(req.params.id);
    const result = await User.findByPk(id);
    res.status(200).json({ user: result });
});

export default router;