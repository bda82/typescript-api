export const db_host = String(process.env.POSTGRES_HOST || 'localhost');
export const db_port = Number(process.env.POSTGRES_PORT || 5432);
export const db_name = String(process.env.POSTGRES_DB || 'tsapp');
export const db_user = String(process.env.POSTGRES_USER || 'postgres');
export const db_password = String(process.env.POSTGRES_PASSWORD || 'postgres');
