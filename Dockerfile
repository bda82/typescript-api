FROM node:16.15-alpine3.14 as base

WORKDIR /app

COPY package*.json /app/

EXPOSE 3001

FROM base as ev

ENV NODE_ENV=development

RUN yarn install

COPY . .

RUN yarn build

CMD ["yarn", "start"]